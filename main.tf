terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.14.9"
}

provider "aws" {
  profile = "default"
  region  = "us-east-1"
}

resource "aws_instance" "sonarqube_instance" {
  ami = "ami-053b0d53c279acc90"
  instance_type = "t2.medium"
  key_name = "aws-key"
  tags = {
    Name = "SonarQube_Instance"
  }
  user_data = filebase64("sonarqube.sh")
  security_groups = [ "FortressGuard" ]
}

resource "aws_security_group" "FortressGuard" {
  name = "FortressGuard"
  description = "The only port avaliable it going to be 9000 because SonarQube!"

  ingress { #pode ouvir outras redes ao redor do mundo
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
    from_port        = 9000
    to_port          = 9000
    protocol         = "tcp"
  }
  egress { #responde requisicoes que chegam no servidor
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
  }

  tags = {
    Name        = "FortressGuard"
  } 
  
}